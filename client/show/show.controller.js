(function () {
    angular
        .module("EmployeeApp")
        .controller("ShowCtrl", ShowCtrl);

    ShowCtrl.$inject = ["$http","$stateParams"];

    function ShowCtrl($http, $stateParams) {
        console.log( $stateParams.empNo);

        var vm = this;
        vm.editorEnabled = false;
        vm.editableFirstName = "";

        vm.empNo = 0;
        vm.result = null;
        vm.status = {}; 

        vm.search = function () {

            $http.get("/api/employeeInfo/", {
                    params: {
                        'emp_no': vm.empNo
                    }
                })
                .then(function (result) {
                    console.info("success");
                    vm.result = result.data;

                }).catch(function () {
                console.info("error");
                vm.status.message = "Failed to get the employee salary from the database.";
                vm.status.code = 400;
            });
        };

        vm.enableEditor = function () {
            vm.editorEnabled = true;
            vm.editableFirstName = vm.result.first_name;
        };

        vm.disableEditor = function () {
            vm.editorEnabled = false;
        };

        vm.save = function () {
            vm.result.first_name = vm.editableFirstName;

            $http.put("/api/employeeInfo/",
                {
                    'emp_no': vm.empNo,
                    'emp_name': vm.editableFirstName
                })
                .then(function (result) {
                    console.info("success");
                    vm.result = result.data;

                }).catch(function () {
                console.info("error");
                vm.status.message = "Failed to set the employee name in the database.";
                vm.status.code = 400;
            });

            vm.disableEditor();
        };

        vm.deleteSalary = function () {
            
            $http.delete("/api/deleteSalary/"+vm.empNo)
                .then(function (result) {
                    console.info("success");
                    vm.search();

                }).catch(function (error) {
                console.info(">> error: %s", JSON.stringify(error));
            });

        };
        
        if($stateParams.empNo){
            vm.empNo=Number($stateParams.empNo);
            vm.search();
        }
    };
})();

(function() {
    "use strict";
    var SearchCtrl = function($state) {
        var vm = this;
        vm.showEmployee=function (empNo) {
            var clickedEmpNo=empNo  ;
            $state.go("C1",{empNo : clickedEmpNo});
        }


    };
    var ShowCtrl= function($stateParams) {
        var vm = this;
        if($stateParams.empNo){
            vm.empNo=Number($stateParams.empNo);
            vm.search();
        }


    };
})();



