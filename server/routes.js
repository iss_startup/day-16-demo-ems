/**
 * Created by vishnu on 30/10/16.
 */
var EmployeeController = require('./api/employee/employee.controller');
var SalaryController = require('./api/salary/salary.controller');

function initRouteHandler(app) {

    app.get("/api/employeeInfo", EmployeeController.get);
    app.put("/api/employeeInfo/", EmployeeController.update);
    app.post('/api/employee', EmployeeController.create);
    app.get('/api/employee', EmployeeController.list);

    app.delete("/api/deleteSalary/:empNo", SalaryController.delete);
    app.get('/api/employeeSalary', SalaryController.get);
}

function errorHandler(app) {
    app.use(function (req, res) {
        res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
    });

    app.use(function (err, req, res, next) {
        res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
    });

}

module.exports = {
    init: initRouteHandler,
    errorHandler: errorHandler
}
