/**
 * Created by vishnu on 30/10/16.
 */
var Employees = require('../../database').Employees;
var Salaries = require('../../database').Salary;
var Titles = require('../../database').Title;

module.exports = {
    list: function (req, res) {

        var employeeName = req.query.emp_name;
        var employeeDBName = req.query.db_name;
        var where = {};
        where[employeeDBName] = employeeName;
      
        Employees
            .findAll({
                where: where,
                limit: 10
            })
            .then(function (result) {

                if (result) {
                    res.json(result);
                } else {
                    res.status(400).send(JSON.stringify("Record Not Found"));
                }
            });

    },
    get: function (req, res) {
        var where = {};
        where.emp_no = req.query.emp_no;
        Employees
            .find({
                where: where,

                include: [{
                    model: Salaries,
                    order: [
                        ['from_date', 'DESC'],
                        ['emp_no', 'ASC']
                    ],
                    limit: 10
                }
                ]
            })
            .then(function (result) {
                if (result && result.dataValues) {
                    Titles.find({
                            where: where
                        })
                        .then(function (response) {
                            if (response && response.dataValues) {
                                result.dataValues.title = response.dataValues.title;
                                res.json(result.dataValues);
                            } else {
                                res.json(result.dataValues);
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        })

                } else {
                    res.status(400).send(JSON.stringify("Record Not Found"));
                }
            });

    },
    update: function (req, res) {
        console.log(req.body);
        var where = {};
        where.emp_no = req.body.emp_no;
        Employees.find(
            {
                where: where
            })
            .then(function (result) {
                if (result) {
                    result.updateAttributes(
                        {
                            first_name: req.body.emp_name
                        }
                    ).then(function (data1) {
                        console.log(data1);
                    })
                }

            });
    },

    create: function (req, res) {
        console.log("hihi");


        //var emp_no = genereateEmpNo(); //alter emp_no to be AUTOINCREMENT for now.

        var gender = req.body.gender == "male" ? 'M' : 'F'; // enum value in the table
        var birth = req.body.birthday.substring(0, req.body.birthday.indexOf('T')); // format date
        var hire = req.body.hiredate.substring(0, req.body.hiredate.indexOf('T')); // format date
        var lastname = req.body.lastname;
        var firstname = req.body.firstname;
        var emp_no = req.body.empNo;


        Employees.create({
            emp_no: emp_no,
            birth_date: birth,
            first_name: firstname,
            last_name: lastname,
            gender: gender,
            hire_date: hire

        }).then(function (result) {
            console.log('--------------', result);
            if (result && result.dataValues) {
                res.json(result.dataValues);
            } else {
                console.log('-------------------')
                throw new Error({error: [{message: 'Record ready in use!'}]});
                res.status(400).send(JSON.stringify("Record Not Found"));
            }

        }).catch(function (error) {
            console.log('---------error------------', error)
            res.status(400).send({error: "Record Already in Use"});
        })

    }

}