//Create a modal for employees table
module.exports = function (sequelize, Sequelize) {
    var Employees = sequelize.define('employees', {
            emp_no: {
                type: Sequelize.INTEGER,
                primaryKey: true
            },
            birth_date: Sequelize.DATE,
            first_name: Sequelize.STRING,
            last_name: Sequelize.STRING,
            gender: Sequelize.STRING,
            hire_date: Sequelize.DATE

        }, {
            timestamps: false
        }
    );
    return Employees;
}

