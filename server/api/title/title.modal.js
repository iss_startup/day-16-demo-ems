//Create a modal for employees table
module.exports = function (sequelize, Sequelize) {
    var Titles = sequelize.define('titles', {
        emp_no: {
            type:Sequelize.INTEGER,
            primaryKey: true
        },
        title: Sequelize.STRING,
        from_date: Sequelize.DATEONLY,
        to_date: Sequelize.DATEONLY

    }, {
        timestamps: false
    });
    return Titles;
}

